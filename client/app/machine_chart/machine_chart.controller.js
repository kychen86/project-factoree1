(function () {
    angular
        .module("Factoree")
        .controller("ChartCtrl", ChartCtrl)

    ChartCtrl.$inject = ['PassportSvc','user','$stateParams'];
    function ChartCtrl(PassportSvc, user, $stateParams) {
        var vm = this;
            vm.page_access = user;
            vm.id;
            vm.result={};
            vm.img = vm.result.machine_image_uri;
            vm.search = search;
            
   function search() {
            PassportSvc
                .getMachineById(vm.id)
                .then(function (result) {
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));
                    console.log(result);
                    vm.result.id = result.data.id;
                    console.log("TEST1");
                    vm.result.machine_name = result.data.machine_name;
                    console.log("TEST2");
                    vm.result.machine_image_uri = result.data.machine_image_uri;
                    console.log("TEST3");
                    vm.result.machine_sequence = result.data.machine_sequence;
                    console.log("TEST4");
                    vm.result.product_id = result.data.product_id;
                    console.log("TEST5");
                    vm.result.sensor_name = result.data.p_nodes[0].sensor_name;
                    console.log(vm.result.sensor_name);
                    vm.result.sensor_image_uri = result.data.p_nodes[0].sensor_image_uri;
                    console.log(vm.result.sensor_image_uri);
                    vm.result.node_serial_num = result.data.p_nodes[0].node_serial_num;
                    console.log(vm.result.node_serial_num);


                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        }

        if ($stateParams.id) {
            console.log("Id passed in",$stateParams.id);
            vm.id = $stateParams.id;
            console.log("Id passed in vm.id",vm.id);
            vm.search();
        }
    }
})();
