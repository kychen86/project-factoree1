(function () {
  angular
    .module('Factoree')
    .controller('NavCtrl', NavCtrl);

  NavCtrl.$inject = ['user', 'PassportSvc'];

  function NavCtrl(user, PassportSvc) {
    var vm = this;

    vm.user = user;
    vm.company_name = "";
    vm.visible = visible;
    vm.sidebarInit = sidebarInit;
    vm.listMachines = false;
    vm.showMachines = showMachines;

    if (user) {
      vm.company_name = user.company_name.toUpperCase();
    }

    if (vm.user.role_id === 1) {
      vm.advanced = true;
    } else {
      vm.advanced = false;
    };

    vm.sidebar_on = false;

    function visible() {
      vm.sidebar_on = !(vm.sidebar_on);
      if (vm.sidebar_on == true) {
        sidebarInit();
      }
    }

    // Function declaration and definition 
    function sidebarInit() {
      console.log("-->init sidebar", vm.user.company_id);
      PassportSvc
        .getProduct_lineList()
        .then(function (results) {
          // The result returned by the DB contains a data object, which in turn contains the records read
          // from the database
          vm.product_lines = results.data;
          console.log(vm.product_lines);
        })
        .catch(function (err) {
          // We console.log the error. For a more graceful way of handling the error, see
          // register.controller.js
          console.log("error " + err);
        });
    }
    function showMachines(){
      vm.listMachines=!(vm.listMachines);
    }

  }//End of NavCtrl
})(); //End of IFFE
