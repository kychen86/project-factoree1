(function () {
    'use strict';
    angular
        .module("Factoree")
        .controller("ProfileCtrl", ProfileCtrl);

    ProfileCtrl.$inject = ["$filter", "PassportSvc", "$stateParams", '$state', 'user'];

    function ProfileCtrl($filter, PassportSvc, $stateParams, $state, user) {

        var vm = this;

        vm.page_access = user;
        var id = user.id;
        vm.result = {};
        vm.fail_msg = "";
        vm.pass_msg = "";

        // Exposed functions 
        vm.search = search;
        vm.toggleEditor = toggleEditor;
        vm.updateUserField = updateUserField;
        vm.removeUser = removeUser;
        vm.searchGatewayList = searchGatewayList;

        // Function declaration and definition -------------------------------------------------------------------------        
        vm.result.id = "";
        vm.result.first_name = "";
        vm.result.last_name = "";
        vm.result.email = "";
        vm.result.phone = "";
        vm.result.sign_up = "";
        vm.result.company_id = "";
        vm.result.role_id = "";
        vm.result.password_hash = "";

        vm.emailHead = "";
        vm.coy_email = "";
        vm.isEditorOn = false;

        search();
        console.log("Result Email", vm.result.email);
        var old_email = vm.result.email;
        console.log("Old Email", old_email);
        var atpos_old = old_email.indexOf("@");
        vm.coy_email = old_email.slice(atpos_old);
        vm.emailHead = old_email.slice(0, atpos_old);

        // when click save ---
        function updateUserField() {
            console.log("-- show.controller.js > save()");

            PassportSvc
                .updateUser(id, vm.result)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                    vm.pass_msg = "Changes are successful"
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                    vm.fail_msg = "Changes are not successful"
                });

        }

        function search() {
            console.log("-- show.controller.js > search");

            PassportSvc
                .getUserById(id)
                .then(function (result) {
                    // This is a good way to understand the type of results you're getting
                    console.log(JSON.stringify(result));
                    console.log("-- show > search > results: \n" + JSON.stringify(result.data));

                    // Exit if result data is empty
                    // if (!result.data) {
                    //     return;
                    // }

                    vm.result.id = result.data.id;
                    vm.result.first_name = result.data.first_name;
                    vm.result.last_name = result.data.last_name;
                    vm.result.email = result.data.email;
                    vm.result.phone = result.data.phone;
                    vm.result.sign_up = result.data.sign_up;
                    vm.result.company_id = result.data.company_id;
                    vm.result.role_id = result.data.role_id;
                    vm.result.password_hash = result.data.password_hash;

                    if(vm.result.role_id ==1){
                        vm.role_type = "Advance"
                    }else{vm.role_type = "Basic"}

                    //MUST ASK FOR HELP

                })
                .catch(function (err) {
                    console.log("--  show > search > error: \n" + JSON.stringify(err));
                });
        }

        function removeUser() {
            console.log("Removing user", vm.result);
            PassportSvc
                .removeUserById(vm.result.id)
                .then(function (result) {
                    console.log("User removed");
                    $state.go('login');
                })
                .catch(function (err) {
                    console.log("Unable to remove user" + JSON.stringify(err));
                });
        }

        // Switches editor state of the input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

        //testing of function  to be moved
        function searchGatewayList() {
            //console.log(vm.searchStringBrand);
            PassportSvc
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .getGatewayList()
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log("results: " + JSON.stringify(results.data));
                    vm.gateway = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.info("error " + JSON.stringify(err));
                });
        }



    }// End of EditCtrl
})();