
(function() {
  angular
    .module('Factoree')
    .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = [ 'PassportSvc', '$state' ];

  function LoginCtrl(PassportSvc, $state) {
    var vm = this;

    vm.user = { //reminder, to be removed
      username: '',
      password: '',
    }
    vm.msg = '';
    
    vm.login = login;


    function login() {
      console.log("Login Controller activated");

      PassportSvc.login(vm.user)
        .then(function(result) {
          console.log(result.data.user.company_id);
          company_id = result.data.user.company_id;
          $state.go("home");
          return true;
        })
        .catch(function(err) {
          vm.msg = 'Invalid Username or Password!';
          vm.user.username = vm.user.password = '';
          return false;
        });
    }
  }
})();
