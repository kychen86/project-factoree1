(function () {
    angular
        .module("Factoree")
        .config(uiRouteConfig);

    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uiRouteConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("login", {
                url: "/login",
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: 'app/login/login.html',
                        controller: 'LoginCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('coy_reg', {
                url: '/coy_reg',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/coy_reg/coy_reg.html",
                        controller: 'Coy_regCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('registration', {
                url: '/registration',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/user_registration/registration.html",
                        controller: 'RegCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('home', {
                url: '/home',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/home/home.html",
                        controller: 'HomeCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("edit", {
                url: "/edit/:id",
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: 'app/user_edit/edit.html',
                        controller: 'EditCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("profile", {
                url: "/profile",
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: 'app/user_profile/profile.html',
                        controller: 'ProfileCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('gateway', {
                url: '/gateway',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/gateway/gateway.html",
                        controller: 'QrCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('node_registration', {
                url: '/node_registration',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/node/node.html",
                        controller: 'NodeCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('node_view', {
                url: '/node_view',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/node_view/node_view.html",
                        controller: 'NodeviewCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('node_settings', {
                url: '/node_settings/:id',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/node_settings/node_settings.html",
                        controller: 'NodeSettingsCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('machine_chart', {
                url: '/machine_chart/:id',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/machine_chart/machine_chart.html",
                        controller: 'ChartCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state('process', {
                url: '/process/:id',
                views: {
                    'nav': {
                        templateUrl: 'app/nav/nav.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/process/process.html",
                        controller: 'ProcessCtrl as ctrl',
                    }
                },
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                }
            });
            
        $urlRouterProvider.otherwise("/login");

    }

})();
