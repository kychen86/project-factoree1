(function () {
    angular
        .module("Factoree")
        .controller("HomeCtrl", HomeCtrl);

    HomeCtrl.$inject = ['PassportSvc', '$state', 'user', '$stateParams'];

    function HomeCtrl(PassportSvc, $state, user, $stateParams) {
        var vm = this;
        vm.gateway_id = "";
        vm.page_access = user;
        vm.processSelected = false;
        // Exposed functions 
        vm.select = select;
        vm.deleteProduct_line = deleteProduct_line;
        // Initializations 
        init();
        // Function declaration and definition 
        function init() {
            PassportSvc
                .getProduct_lineList()
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.product_lines = results.data;
                    console.log(vm.product_lines);
                    console.log(results.data.length);
                    if (results.data.length <= 1) {
                        $state.go('gateway');
                    }
                })
                .catch(function (err) {

                    console.log("error " + err);
                });
        }
        function select(id) {
            vm.processSelected = true;
            $state.go("process", { id: id });
        }

        function deleteProduct_line(product_line_id) {
            console.log("Removing Product Line");
            PassportSvc
                .removeProduct_line(product_line_id)
                .then(function (product_line_id) {
                    console.log("process removed", product_line_id);
                    vm.delete_msg = "product_line_id removed";
                    init();
                })
                .catch(function (err) {
                    console.log("Unable to remove process" + JSON.stringify(err));
                });
        }

        //        if ($stateParams.id) {
        //     console.log("Id passed in", $stateParams.id);
        //     vm.id = $stateParams.id;
        //     console.log("Id passed in vm.id", vm.id);
        //     init();
        // }

    }
})();
