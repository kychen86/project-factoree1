(function () {
    angular
        .module("Factoree")
        .controller("NodeviewCtrl", NodeviewCtrl);

    NodeviewCtrl.$inject = ['PassportSvc', '$state', 'user'];

    function NodeviewCtrl(PassportSvc, $state, user) {
        var vm = this;
        vm.gateway_id = "";
        vm.page_access = user;
        vm.node_serial_num = "";
        vm.sensor_name = "";
        vm.machine_name = "";
        vm.line_name = "";
        vm.sig_type = "";
        vm.status = "";


        // Exposed functions 
        vm.goToEdit = goToEdit; //Edit Node
        vm.search = search;
        vm.deleteGateway = deleteGateway;
        // Initializations 
        init();


        // Function declaration and definition 
        function search(gateway_id, gate_serial_num) {
            vm.choosenGateway = gate_serial_num.toUpperCase();
            PassportSvc
                .getNodesByGatewayId(gateway_id)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.nodes = results.data;
                    console.log(vm.nodes);

                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }
        function init() {
            PassportSvc
                .getGatewayList()
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.gateways = results.data;
                    console.log(vm.gateways);
                })
                .catch(function (err) {

                    console.log("error " + err);
                });
        }

        function goToEdit(id) {
            $state.go("node_settings", { id: id });
        }

        function deleteGateway(gateway_id) {
            console.log("Removing Gateway");
            PassportSvc
                .removeGateway(gateway_id)
                .then(function (gateway_id) {
                    console.log("gateway removed",gateway_id);
                    vm.delete_msg = "gateway_id removed";
                    init();
                })
                .catch(function (err) {
                    console.log("Unable to remove gateway" + JSON.stringify(err));
                });
        }
    }
})();
