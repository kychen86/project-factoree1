(function () {
  angular
    .module('Factoree')
    .service('PassportSvc', PassportSvc);

  PassportSvc.$inject = ["$http"];

  function PassportSvc($http) {
    var svc = this;

    // EXPOSED FUNCTIONS 
    svc.login = login;
    svc.userAuth = userAuth;
    //user
    svc.createUser = createUser;
    svc.getAllUser = getAllUser;
    svc.getUserById = getUserById;
    svc.getUserByEmail = getUserByEmail;
    svc.updateUser = updateUser;
    svc.removeUserById = removeUserById;
    //company
    svc.createCompany = createCompany;
    svc.getCompanyById = getCompanyById;
    svc.updateCompanyById = updateCompanyById;
    svc.removeCompany = removeCompany;
    svc.getUnassigned = getUnassigned;
    //gateway
    svc.createGateway = createGateway;
    svc.getGatewayById = getGatewayById;
    svc.getGatewayList = getGatewayList;
    svc.removeGateway = removeGateway;
    svc.getLatestGatewayByCompany = getLatestGatewayByCompany;
    //product_line
    svc.createProduct_line = createProduct_line;
    svc.getProduct_lineById = getProduct_lineById;
    svc.getProduct_lineList = getProduct_lineList;
    svc.removeProduct_line = removeProduct_line;
    svc.updateProduct_line = updateProduct_line;
    //machine
    svc.createMachine = createMachine;
    svc.getMachineById = getMachineById;
    svc.getMachineList = getMachineList;
    svc.updateMachine = updateMachine;
    //node
    svc.captureImage = captureImage;
    svc.getNodesByGatewayId = getNodesByGatewayId;
    svc.getNodeById = getNodeById;
    svc.createNode = createNode;
    svc.updateNode = updateNode;
    svc.updateFirebaseDB = updateFirebaseDB;

    //Function Definition
    function userAuth() {
      return $http.get(
        '/user/auth',
      );
    }
    function login(user) {
      console.log("------", user);
      return $http.post(
        '/login', {
          email: user.username,
          password_hash: user.password
        }
      );
    }
    //User
    function createUser(user) {
      console.log("Create------", user);
      return $http.post(
        'api/user', user
      );
    }
    function getAllUser() {
      return $http({
        method: 'GET',
        url: 'api/user',
      });
    }
    function getUserById(id) {
      return $http({
        method: 'GET',
        url: "api/user/" + id
      });
    }
    function getUserByEmail(email) {
      return $http({
        method: 'GET',
        url: 'api/user',
        params: {
          'email': email,
        }
      });
    }
    function updateUser(id, user) {//pass in user.id and user as a whole
      //console.log("what is in user for update",user);
      return $http({
        method: 'PUT',
        url: 'api/user/' + id,
        data: user //can actully pass in the whole json object i think
      });
    }
    function removeUserById(id) {
      return $http({
        method: 'DELETE',
        url: 'api/user/' + id,
      });
    }

    //End of User
    //Start of Company
    function createCompany(company) { //coy_reg.html
      return $http.post(
        'api/company', company
      );
    }
    function getCompanyById(id) {
      return $http({
        method: 'GET',
        url: 'api/company/' + id,
      });
    }
    function updateCompanyById(id, coy_name, coy_email) {
      return $http({
        method: 'PUT',
        url: 'api/company/' + id,
        data: {
          id: id,
          coy_name: coy_name,
          coy_email: coy_email
        }
      });
    }
    function removeCompany(id) {
      return $http({
        method: 'DELETE'
        , url: 'api/company/' + id,
      });
    }
    //End of Company
    //Gateway
    function createGateway(gateway) {
      return $http.post(
        'api/gateway', gateway
      );
    }
    function getGatewayById(id) {
      return $http({
        method: 'GET',
        url: 'api/gateway/' + id
      });
    }
    function getGatewayList() { //by company_id
      return $http({
        method: 'GET',
        url: 'api/gateway',
      });
    }
    function getLatestGatewayByCompany() {
      console.log("In passport");
      return $http({
        method: 'GET',
        url: 'api/gateway/company',
      });
    }
    function removeGateway(id) {
      console.log("Passport removeGateway");
      return $http({
        method: 'DELETE',
        url: 'api/gateway/' + id,
      });
    }
    //End of gateway Since gateway only has 2 fields, connected to company and its s/n, no update required
    //Node
    function captureImage(imageToUpload) {
      return $http.post(
        '/snapshot', imageToUpload
      );
    }
    function createNode(node) {
      return $http.post(
        'api/p_node', node
      );
    }
    function getNodesByGatewayId(gateway_id) {
      console.log(gateway_id);
      return $http({
        method: 'GET',
        url: 'api/p_node/gateway',
        params: {
          'gateway_id': gateway_id,
        }
      });
    }
    function getNodeById(id) {
      return $http({
        method: 'GET',
        url: 'api/p_node/' + id
      });
    }
    function updateNode(id, node) {
      console.log("Node", node);
      return $http({
        method: 'PUT',
        url: 'api/p_node/' + id,
        data: node
      });
    }

    function updateFirebaseDB(id, node) {
      console.log("Node", node);
      return $http({
        method: 'PUT',
        url: 'api/node/' + id,
        data: node
      });
    }

    //End of Node
    //Machine
    function createMachine(machine) {
      return $http.post(
        'api/machine/', machine
      );
    }
    function getMachineById(id, machine) {
      return $http({
        method: 'GET',
        url: 'api/machine/' + id
      });
    }
    function getMachineList(product_line_id) {
      return $http({
        method: 'GET',
        url: 'api/machine/product/',
        params: {
          'product_line_id': product_line_id,
        }
      });
    }
    function updateMachine(id, machine) {
      console.log("Passport: updateMachine:", machine)
      return $http({
        method: 'PUT',
        url: 'api/machine/' + id,
        data: machine
      });
    }
    //End of Machine
    //Product Line
    function createProduct_line(product_line) {
      return $http.post(
        'api/product_line', product_line
      );
    }
    function getProduct_lineById(id) {
      return $http({
        method: 'GET',
        url: 'api/product_line/' + id
      });
    }
    function getProduct_lineList() {
      return $http({
        method: 'GET',
        url: 'api/product_line',
      });
    }
    function removeProduct_line(id) {
      return $http({
        method: 'DELETE',
        url: 'api/product_line/' + id,
      });
    }
    function updateProduct_line(id, product_line) {
      return $http({
        method: 'PUT',
        url: 'api/product_line/' + id,
        data: product_line
      });
    }
    function getUnassigned() {
      return $http({
        method: 'GET',
        url: 'api/product_line/unassigned'
      });
    }
    //End of Product Line
  }//End of PassportSvc
})();
