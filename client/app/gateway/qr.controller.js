(function () {
    angular
        .module("Factoree")
        .controller("QrCtrl", QrCtrl)

    QrCtrl.$inject = ['PassportSvc', '$state', 'user'];

    function QrCtrl(PassportSvc, $state, user) {
        var vm = this;
        vm.img = "/assets/images/qrscan.png";
        vm.page_access = user;
        vm.msg = "";
        vm.trackerImg = "/assets/images/tracker1.png";

        vm.start = start;
        vm.submit = submit;
        vm.processSNfromQR = processSNfromQR;

        vm.gateway = {};
        vm.gateway.company_id = user.company_id;

        function start() {
            vm.cameraRequested = true;
        }

        function processSNfromQR(gatewaySN) {
            vm.gateway.gatewaySN = gatewaySN;
            vm.cameraRequested = false;
            console.log("gatewaySN:",vm.gateway.gatewaySN);
            submit(); //Validation check in submit function DONE
        }

        function submit() {
            console.log("Gateway Detected");
            var str = vm.gateway.gatewaySN;
            var regex = /^[0-9a-fA-F]{16}$/;
            var valid_num = regex.exec(str);
            //console.log(valid_num);
            if (valid_num) {
                PassportSvc
                    .createGateway(vm.gateway)
                    .then(function (result) {
                        console.log("submitted");
                        $state.go('node_registration');
                        return true;
                    })
                    .catch(function (err) {
                        console.log(err);
                        vm.msg = err.data.error;
                        vm.gateway.gatewaySN = '';
                        return false;
                    });
            }
            else {
                console.log("Error");
                vm.msg = "Error detected non valid serial number, please contact support.";
            }

        }

    }

})();
