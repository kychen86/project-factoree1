(function () {
    angular
        .module("Factoree")
        .controller("Coy_regCtrl", Coy_regCtrl);

    Coy_regCtrl.$inject = ['PassportSvc', '$state', 'user'];

    function Coy_regCtrl(PassportSvc, $state, user) {
        var vm = this;
        var login = user;
        vm.fail_msg = "";
        vm.pass_msg = "";      
        vm.email = "";
        vm.page_access = false;

        if((login.company_id == 1)&&(login.role_id == 1)){
            vm.page_access = true;
        };

        vm.company = {
            coy_name: "",
            coy_registry: "",
            coy_email: "",
        };

        // Exposed functions 
        vm.coy_regButton = coy_regButton;

        function coy_regButton() {
            console.log("Register Company");
            var atpos = vm.email.indexOf("@");
            vm.company.coy_email = vm.email.slice(atpos);
            console.log(vm.company.coy_email);
            var str = vm.company.coy_registry;
            var regex = /^\d{9}[a-zA-Z]$/;
            var valid_num = regex.exec(str);
            //console.log(valid_num);
            if (valid_num) {
                PassportSvc
                    .createCompany(vm.company)
                    .then(function (result) {
                        console.log("submitted");
                       vm.pass_msg = "Company Registration Successful"
                        return true;
                    })
                    .catch(function (err) {
                        console.log(err);
                        vm.fail_msg = err.data.error;
                        vm.company = {};
                        return false;
                    });
            }
            else {
                console.log("Error");
                vm.fail_msg = "Error detected non valid UEN number, please contact support.";
            }

        }

    }// END RegCtrl
})();
