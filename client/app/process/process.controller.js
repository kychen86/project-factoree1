(function () {
    angular
        .module("Factoree")
        .controller("ProcessCtrl", ProcessCtrl)

    ProcessCtrl.$inject = ['PassportSvc','user', '$stateParams'];
    function ProcessCtrl(PassportSvc, user, $stateParams) {
        var vm = this;
            vm.page_access = user;
            vm.id;
            vm.search = search;

     function search() {
            PassportSvc
                .getMachineList(vm.id)
                .then(function (result) {
                    //console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));
                    //console.log(result);
                    vm.results = result.data;
                    vm.product_name = result.data[0].product_line.line_name;

                    
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        }
    
            if ($stateParams.id) {
            console.log("Id passed in",$stateParams.id);
            vm.id = $stateParams.id;
            console.log("Id passed in vm.id",vm.id);
            vm.search();
        }

    }
})();
