(function () {
    angular
        .module("Factoree")
        .controller("NodeCtrl", NodeCtrl)

    NodeCtrl.$inject = ['PassportSvc', '$state', 'user', '$http', 'Upload'];
    function NodeCtrl(PassportSvc, $state, user, $http, Upload) {

        var vm = this;
        vm.page_access = user;
        vm.img = "/assets/images/qrscan.png";
        vm.img2 = "/assets/images/camera.svg";
        vm.trackerImg = "/assets/images/tracker2.png";
        vm.sensorPic = null;
        vm.machinePic = null;
        vm.pass_msg = "";
        vm.fail_msg = "";

        vm.submitNode = submitNode;
        vm.submitMachine = submitMachine;

        vm.processSNfromQR = processSNfromQR;
        vm.startImage = startImage;
        vm.stopImage = stopImage;
        vm.sendSnapshotToServer = sendSnapshotToServer;
        vm.makeSnapshot = makeSnapshot;
        vm.createProduct = createProduct;
        vm.saveButton = saveButton;

        vm.node = {};
        vm.node.node_serial_num = "";
        vm.node.sensor_name = "";
        vm.node.sensor_image_uri = null;
        vm.node.sig_type = "3";
        vm.node.sig_0 = "1";
        vm.node.sig_1 = "0";
        vm.node.sig_2 = "0";
        vm.node.sig_3 = "0";
        vm.node.sig_4 = "0";
        vm.node.sig_5 = "0";
        vm.node.vmax = "24";
        vm.node.pushint = "1";
        vm.node.gateway_id = "";
        vm.node.machine_id = ""; //take from unassigned

        vm.machine = {};
        vm.machine.machine_name = "";
        vm.machine.machine_image_uri = null;
        vm.machine.machine_sequence = "1";
        vm.machine.product_line_id = ""; //take from unassigned

        vm.product_line = {};
        vm.product_line.line_name = "";

        vm.nodeCreated = false;
        vm.machineCreated = false;
        vm.lineCreated = false;

        vm.registerNewProduct = registerNewProduct;
        vm.newProduct = false;
        vm.existingProduct_line_id = "";
        vm.skipProcessCreation = skipProcessCreation;

        vm.registerNewMachine = registerNewMachine;
        vm.newMachine = false;
        vm.existingMachine_id = "";
        vm.skipMachineCreation = skipMachineCreation;

        var product_line_id = "";
        var machine_id = "";

        var isSensor = false;
        var isMachine = false;
        //make snapshot of sensor when capture is pressed
        vm.sensorButton = function () {
            isSensor = true;
            isMachine = false;
            makeSnapshot();
        }
        //make snapshot of machine when capture is pressed
        vm.machineButton = function () {
            isMachine = true;
            isSensor = false;
            makeSnapshot();
        }

        init();
        //---------QR-----------
        vm.startQR = function () {
            vm.cameraRequestedQR = true;
        }

        function processSNfromQR(nodeSN) {
            vm.node.nodeSN = nodeSN;
            vm.cameraRequestedQR = false;
            console.log("nodeSN:", vm.node.nodeSN);
        }

        function startImage() {
            vm.cameraRequestedImage = true;
        }

        function stopImage() {
            vm.cameraRequestedImage = false;
        }

        //______________________Activate Camera______________________________
        var _video = null,
            patData = null,
            activeStream = null;

        var prepImage = null;

        vm.patOpts = {};
        vm.isTurnOn = false;
        vm.webcamError = false;

        vm.channel = {};

        vm.onError = function (err) {
            // console.log("onError");
            console.log(err);
        }

        vm.onStream = function (stream) {
            // console.log("onStream");
            activeStream = stream;
        }

        vm.onSuccess = function () {
            // console.log("onSuccess");
            _video = vm.channel.video;
            vm.patOpts.w = _video.width;
            vm.patOpts.h = _video.height;
            vm.isTurnOn = true;
        }

        var getVideoData = function getVideoData(x, y, w, h) {
            var hiddenCanvas = document.createElement('canvas');
            hiddenCanvas.width = _video.width;
            hiddenCanvas.height = _video.height;
            var ctx = hiddenCanvas.getContext('2d');
            ctx.drawImage(_video, 0, 0, _video.width, _video.height);
            return ctx.getImageData(x, y, w, h);
        };

        // function upload(vm.sensorImage) {
        //     // Upload configuration and invokation 
        //     Upload
        //         .upload({
        //             url: '/snapshot',
        //             imageToUpload
        //         })
        //         .then(function (result) {
        //             console.log(result.data);
        //         })
        //         .catch(function (err) {
        //             console.log(err);
        //         });
        // };


        function sendSnapshotToServer() {
            vm.snapshotData = prepImage;
            //console.log("____SNAPSHOT______", vm.snapshotData);

            // get the extension
            var ext = vm.snapshotData.split(';')[0].match(/jpeg|png|gif/)[0];
            // strip off the data: url prefix to get just the base64-encoded bytes
            var data = vm.snapshotData.replace(/^data:image\/\w+;base64,/, "");

            //set up data to upload to server
            if (isSensor) {
                var imageToUpload = {
                    filename: 'sensor.' + ext,
                    imagedata: data,
                }
                console.log("Sending Sensor Image to server");
            } else if (isMachine) {
                var imageToUpload = {
                    filename: 'machine.' + ext,
                    imagedata: data,
                }
                console.log("Sending Sensor Image to server");
            } else { console.log("Error, Nothing to send") }

            //var imageToUpload = {filename:'test'}
            console.log("imageToUpload", imageToUpload);
            takeImage(imageToUpload);

            function takeImage(imageToUpload) {
                PassportSvc
                    .captureImage(imageToUpload)
                    .then(function (result) {
                        console.log(result.data);
                        console.log(isSensor + "<--sensor, machine-->" + isMachine);
                        if (isSensor) {
                            //vm.sensorPic = result.data.savedAsFile;
                            vm.node.sensor_image_uri = "/snapshots/" + result.data.savedAsFile;
                            console.log(vm.node.sensor_image_uri);

                            console.log("sensor");
                            submitNode()
                                .then(function (result) {
                                    console.log("Create Node", result.data);
                                    console.log("submitted Node");
                                    vm.pass_msg = "Node Registration Successful"
                                    vm.nodeCreated = true;
                                    vm.node.id = result.data.id;
                                    vm.node.node_serial_num = result.data.node_serial_num;
                                    console.log("vm.node.id:", vm.node.id);
                                    return true;
                                })
                                .catch(function (err) {
                                    console.log(err);
                                    vm.fail_msg = err.data.error;
                                    vm.node.nodeSN = '';
                                    return false;
                                });
                            isSensor = false;

                        } else if (isMachine) {
                            //vm.machinePic = result.data.savedAsFile;
                            vm.machine.machine_image_uri = "/snapshots/" + result.data.savedAsFile;
                            console.log(vm.machine.machine_image_uri);
                            console.log("machine");
                            submitMachine()
                                .then(function (result) {
                                    console.log("submitted Machine");
                                    console.log("Create Machine", result.data);
                                    vm.pass_msg = "Machine Record Added"
                                    vm.machineCreated = true;
                                    machine_id = result.data.id;
                                    console.log("machine_id", machine_id);
                                    return true;
                                })
                                .catch(function (err) {
                                    console.log(err);
                                    vm.fail_msg = err.data.error;
                                    return false;
                                });
                            isMachine = false;
                        } else {
                            console.log("This should not be happening");
                        }
                    })
                    .catch(function (err) {
                        vm.fail_msg = "No image attached"
                        console.log(err);
                    });
            };

            // At the server, convert to a file using the following code:      
            // var buf = new Buffer(imagedata, 'base64');
            // fs.writeFile(filename, buf);
        };

        function makeSnapshot() {
            console.log('Capture without sending');
            if (_video) {
                var patCanvas = document.querySelector('#snapshot');
                if (!patCanvas) return;

                patCanvas.width = _video.width;
                patCanvas.height = _video.height;
                var ctxPat = patCanvas.getContext('2d');

                var idata = getVideoData(vm.patOpts.x, vm.patOpts.y, vm.patOpts.w, vm.patOpts.h);
                ctxPat.putImageData(idata, 0, 0);

                console.log('various image not sent to server')

                prepImage = patCanvas.toDataURL();
                //console.log(prepImage);
                //sendSnapshotToServer(patCanvas.toDataURL());
                if (isSensor) {
                    vm.sensorPic = prepImage;
                    console.log("Its a sensor image");
                }
                if (isMachine) {
                    vm.machinePic = prepImage;
                    console.log("Its a machine image");
                }
                vm.patData = idata;
                stopImage();
            }
        }
        //___________________________________________

        //create a row of node in table p_node and insert predefined values
        function submitNode() {
            console.log("Node SN detected");
            var str = vm.node.nodeSN;
            var regex = /^[0-9a-fA-F]{16}$/;
            var valid_num = regex.exec(str);
            //console.log(valid_num);
            if (valid_num) {
                var p = PassportSvc
                    .createNode(vm.node)
                return p;
            }
            else {
                console.log("Error");
                vm.fail_msg = "Error detected non valid serial number, please contact support.";
            }

        }

        //create Machine in database tie it back to sensor
        function submitMachine() {
            console.log("submit Machine Data");
            var p = PassportSvc
                .createMachine(vm.machine)
            return p;
        }

        //Which gateway it is attached to
        function init() {
            PassportSvc
                .getUnassigned()
                .then(function (result) {
                    //machineId of Unassigned
                    console.log(result.data);
                    vm.node.machine_id = result.data["machines.id"];
                    console.log(vm.node.machine_id);
                    vm.machine.product_line_id = result.data.id;
                    console.log(vm.machine.product_line_id);
                })
                .catch(function (err) {
                    console.log(err);
                });
            PassportSvc
                .getLatestGatewayByCompany()
                .then(function (result) {
                    //console.log("result from init,", result);
                    //console.log("result.data,", result.data);
                    vm.gateway = result.data[0];
                    //console.log(vm.gateway);
                    vm.node.gateway_id = vm.gateway.id;
                    console.log("vm.node.gateway_id", vm.node.gateway_id);
                })
                .catch(function (err) {
                    console.log(err);
                });
            PassportSvc
                .getProduct_lineList()
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.product_lines = results.data;
                    console.log(vm.product_lines);
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }

        // create new product_line
        function createProduct() {
            PassportSvc
                .createProduct_line(vm.product_line)
                .then(function (result) {
                    console.log("Create Product", result.data);
                    product_line_id = result.data.id;
                    vm.lineCreated = true;
                    console.log(product_line_id);
                    vm.pass_msg = "Product Line created successfully.";
                })
                .catch(function (err) {
                    console.log("Error", err.data.error);
                    vm.fail_msg = err.data.error;
                });
        }

        //Creating Function for save button, have to update machine entry with new product_line_id and then update, node entry with machine_id
        function saveButton() {
            vm.machine.product_line_id = product_line_id;
            console.log("productLine ID:", vm.machine.product_line_id);
            vm.node.machine_id = machine_id;
            vm.machine.id = machine_id;
            console.log("saveButton");
            PassportSvc
                .updateMachine(machine_id, vm.machine)
                .then(function (result) {
                    console.log("Updated Machine" + result.data);
                    vm.pass_msg = "Machine updated are successful"
                    PassportSvc
                        .updateNode(vm.node.id, vm.node)
                        .then(function (result) {
                            console.log("Updated Node" + result.data);
                            vm.pass_msg = "Node updated are successful"
                            $state.go("node_settings", { id: vm.node.id });
                        })
                        .catch(function (err) {
                            console.log("error: \n" + err);
                            vm.fail_msg = "Node update are not successful"
                        });
                })
                .catch(function (err) {
                    console.log("error: \n" + err);
                    vm.fail_msg = "Machine update are not successful"
                });


        }

        function registerNewProduct() {
            vm.newProduct = true;
        }

        function skipProcessCreation() {
            product_line_id = vm.existingProduct_line_id;
            console.log("ProductLine ID",product_line_id);
            vm.lineCreated = true;
            PassportSvc
                .getMachineList(product_line_id)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.machines = results.data;
                    console.log(vm.machines);
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });

        }

        function skipMachineCreation() {
            machine_id = vm.existingMachine_id;
            vm.machineCreated = true;
        }
        function registerNewMachine() {
            vm.newMachine = true;
        }

    }

})();
