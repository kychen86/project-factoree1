(function () {
    'use strict';
    angular
        .module("Factoree")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "PassportSvc", '$state', 'user'];

    function EditCtrl($filter, PassportSvc, $state, user) {

        var vm = this;

        var login = user;
        vm.id = "";
        vm.result = {};

        vm.page_access = false;

        if((login.company_id)&&(login.role_id == 1)){
            vm.page_access = true;
        };
       
        
        // Exposed functions 
        vm.search = search;
        vm.toggleEditor = toggleEditor;
        vm.updateUserField = updateUserField;
        vm.removeUser = removeUser;
        vm.searchGatewayList = searchGatewayList;

        // Function declaration and definition -------------------------------------------------------------------------        
            vm.result.id = "";
            vm.result.first_name = "";
            vm.result.last_name = "";
            vm.result.email = "";
            vm.result.phone = "";
            vm.result.sign_up = "";
            vm.result.company_id = "";
            vm.result.role_id = "";
            vm.old_email = "";
            vm.showDetails = false;
            vm.isEditorOn = false;
        

        // Saves edited field
        function updateUserField() {
            console.log("-- show.controller.js > save()");
            //Check email criteria still holds

            PassportSvc
                .updateUser(vm.id, vm.result)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                    $state.go('gateway');
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
        }

        function search() {
            console.log("-- show.controller.js > search()");
            vm.showDetails = true;

            PassportSvc
                .getUserById(vm.id)
                .then(function (result) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    // Exit .then() if result data is empty
                    if (!result.data)
                        return;

                    vm.result.id = result.data.id;
                    vm.result.first_name = result.data.first_name;
                    vm.result.last_name = result.data.last_name;
                    vm.result.email = result.data.email;
                    vm.result.phone = result.data.phone;
                    vm.result.sign_up = result.data.sign_up;
                    vm.result.company_id = result.data.company_id;
                    vm.result.role_id = result.data.role.id;
                    vm.old_email = result.data.email;

                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        }

        function removeUser(){
            console.log("Removing user", vm.result);
            PassportSvc
                .removeUserById(vm.result.id)
                .then(function(result){
                    console.log("User removed");
                })
                .catch(function(err){
                    console.log("Unable to remove user" + JSON.stringify(err));
                });
        }

        // Switches editor state of the input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }
    
    //testing of function  to be moved
        function searchGatewayList() {        
            //console.log(vm.searchStringBrand);
            PassportSvc
            // we pass contents of vm.searchString to service so that we can search the DB for this string
                .getGatewayList()
                .then(function (results){
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log("results: " + JSON.stringify(results.data));
                    vm.gateway = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.info("error " + JSON.stringify(err));
                });
        }



    }// End of EditCtrl
})();