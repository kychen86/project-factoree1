(function () {
    angular
        .module("Factoree")
        .controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ['PassportSvc', '$state'];

    function RegCtrl(PassportSvc, $state) {
        var vm = this;
        var todayDate = new Date();
        // EXPOSED DATA MODELS         
        vm.password = "";
        vm.confirmPassword = "";
        vm.coy_name = "";
        vm.fail_msg = "";
        var coy_email = "";

        vm.user = {
            first_name: "",
            last_name: "",
            password_hash: "", //Set to clear for now
            email: "",
            confirmEmail: "",
            phone: "",
            company_id: "",
            sign_up: todayDate,
            role_id: "1",
            reset_password_sent_at: null,
            reset_password_token: null
        };
        // Exposed functions 
        vm.registerButton = registerButton;

        // Function declaration and definition 

        function registerButton() {
            // get email from form and truncate to @zzz.zzz for checking
            // compare using findAll against companies table.
            // if company exist, check user; if company does not exist call support.
            // if user exist, reject registration, send to log in page.
            var atpos = vm.user.email.indexOf("@");
            coy_email = vm.user.email.slice(atpos);
            console.log(coy_email);
            vm.user.password_hash = vm.password;
            PassportSvc
                .createUser(vm.user)
                .then(function (result) {
                        console.log("User Register result " + JSON.stringify(result));
                    $state.go('login');
                })
                .catch(function (err) {
                    console.log("User not created");
                    console.log(err.data)
                    vm.fail_msg = err.data.error;
                });
        }

    }// END RegCtrl
})();
