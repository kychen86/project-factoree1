(function () {
    'use strict';
    angular
        .module("Factoree")
        .controller("NodeSettingsCtrl", NodeSettingsCtrl);

    NodeSettingsCtrl.$inject = ["PassportSvc", "$stateParams", "user"];

    function NodeSettingsCtrl(PassportSvc, $stateParams, user) {

        var vm = this;
        vm.page_access = user;
        vm.trackerImg = "/assets/images/tracker3.png";
        // Exposed data models -----------------------------------------------------------------------------------------

        vm.id = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.

        vm.search = search;
        vm.updateNodeSettings = updateNodeSettings;
        vm.updateMachineSequence = updateMachineSequence;
        vm.saveButton = saveButton;
        vm.updateFirebase = updateFirebase;

        vm.result.id = "";
        vm.result.node_serial_num = "";
        vm.result.line_name = "";
        vm.result.machine_name = "";
        vm.result.machine_sequence = "";
        vm.result.sensor_name = "";
        vm.result.sig_type = "";
        vm.result.sig_0 = "";
        vm.result.sig_1 = "";
        vm.result.sig_2 = "";
        vm.result.sig_3 = "";
        vm.result.sig_4 = "";
        vm.result.sig_5 = "";
        vm.result.vmax = "";
        vm.pushint = "";
        vm.showAdvanced = false;

        vm.machine = {};
        vm.machine.machine_name = "";
        vm.machine.machine_image_uri = null;
        vm.machine.machine_sequence = "1";
        vm.machine.product_line_id = "";


        vm.toggle = function () {
            vm.showAdvanced = !(vm.showAdvanced);
        }

        function saveButton(){
            updateNodeSettings();
            updateMachineSequence();
        }

        // Save Button
        function updateNodeSettings() {
            console.log("-- show.controller.js > save()");
            console.log(vm.result.sensor_name);
            PassportSvc
                .updateNode(vm.id, vm.result)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                    vm.pass_msg = "Update Successful"
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                    vm.fail_msg = "Updated failed, please try again"
                });
        }

        function search() {
            PassportSvc
                .getNodeById(vm.id)
                .then(function (result) {
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    // Exit .then() if result data is empty
                    // if (!result.data)
                    //     return;
                    // The result is an array of objects that contain only 1 object
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    //console.log(result.data["machine.machine_name"]);
                    vm.result.id = result.data.id;
                    //console.log("TEST1");
                    vm.result.node_serial_num = result.data.node_serial_num;
                    //console.log("TEST2");
                    vm.result.line_name = result.data["machine.product_line.line_name"];
                    vm.result.product_line_id = result.data["machine.product_line_id"];
                    //console.log("TEST3");
                    vm.result.machine_name = result.data["machine.machine_name"];
                    vm.result.machine_image_uri = result.data["machine.machine_image_uri"];
                    //console.log("TEST4");
                    vm.result.machine_sequence = result.data["machine.machine_sequence"];
                    //console.log("TEST5");
                    vm.result.sensor_name = result.data.sensor_name;
                    //console.log("TEST6");
                    vm.result.sig_type = result.data.sig_type;
                    vm.result.sig_0 = result.data.sig_0;
                    vm.result.sig_1 = result.data.sig_1;
                    vm.result.sig_2 = result.data.sig_2;
                    vm.result.sig_3 = result.data.sig_3;
                    vm.result.sig_4 = result.data.sig_4;
                    vm.result.sig_5 = result.data.sig_5;
                    vm.result.vmax = result.data.vmax;
                    vm.result.pushint = result.data.pushint;
                    vm.result.machine_id = result.data.machine_id;
                    vm.result.sensor_image_uri = result.data.sensor_image_uri;
                    console.log("TEST7");


                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        }

        function updateMachineSequence() {
                    vm.machine.machine_name = vm.result.machine_name;
                    vm.machine.machine_image_uri = vm.result.machine_image_uri;
                    vm.machine.machine_sequence = vm.result.machine_sequence;
                    vm.machine.product_line_id = vm.result.product_line_id;
                    console.log("vm.machine", vm.machine);             
            PassportSvc
                .updateMachine(vm.result.machine_id, vm.machine)
                .then(function (result) {
                    console.log("Updated Machine" + JSON.stringify(result.data));
                    vm.pass_msg = "Updates successful"
                })
                .catch(function (err) {
                    console.log("error: \n" + err);
                    vm.fail_msg = "Machine update are not successful"
                });

        }

        if ($stateParams.id) {
            console.log("Id passed in", $stateParams.id);
            vm.id = $stateParams.id;
            console.log("Id passed in vm.id", vm.id);
            vm.search();
        }

        function updateFirebase(){
            PassportSvc
            .updateFirebaseDB(vm.id,vm.result)
            .then(function(result){
                console.log("Firebase updated");
            })
            .catch(function(err){
                console.log("Firebase Failed")
            })
        }

    }
})();
