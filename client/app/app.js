(function () {
    angular
        .module("Factoree",
            [
             "ngMessages",            
             "ngAnimate",
             "ui.router",
             "webcam",
             "bcQrReader",
             "ngFileUpload"
            // 
        ]
        );
})();
