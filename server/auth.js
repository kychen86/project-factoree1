
var LocalStrategy = require("passport-local").Strategy;
//var bcrypt   = require('bcryptjs');

var User = require("./db").User;
var Company = require("./db").Company;
var config = require("./config");
//Setup local strategy


module.exports = function (app, passport) {
    function authenticateLocal(username, password, done) {

        User.findOne({
            where: {
                email: username
            }
        }).then(function (result) { //result is from database and information is stored in result.dataValues for findOne function, in findAll, it is stored in an array.
            if (!result) {
                return done(null, false);
            } else {
                //console.log(result.dataValues);
                console.log("END-of-CHECK--USERNAME")
                if (password == result.dataValues.password_hash) {
                    //if(bcrypt.compareSync(password , result.password)){
                    // var whereClause = {};
                    // whereClause.email = username;
                    // User
                    //     .update({ reset_password_token: null },
                    //     { where: whereClause });                    
                    return done(null, result);
                } else {
                    return done(null, false);
                }
            }
        }).catch(function (err) {
            return done(err, false);
        });
    }

    passport.use(new LocalStrategy({
        usernameField: "email", //that which is passed in from client service
        passwordField: "password_hash"
    }, authenticateLocal
    ));

    passport.serializeUser(function (user, done) {
        //console.info("serial to session");
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        //console.log(user.company_id);
        User.findOne({
            where: {
                email: user.email
            }
        }).then(function (result) {
            if (result) {
                findCompanyName(result.company_id)
                    .then(function (result) {
                        //user.company_name = "test";
                        //console.log("result=",result);
                        user.company_name = result.coy_name;
                        user.company_email = result.coy_email;
                        done(null, user);
                    })
                    .catch(function (err) {
                        console.log("err=", err);
                    });

            }
        }).catch(function (err) {
            done(err, user);
        });
    });
    var isAuthenticated = function (req, res, next) {
        //console.log("isAuthenticated(): ", req.user);
        if (req.isAuthenticated()) {
            next();
        }
        else {
            res.sendStatus(401);
        }
    }
    return {
        isAuthenticated: isAuthenticated,
    }
};

function findCompanyName(company_id) {
    var p = Company
        .findOne({
            where: {
                id: company_id
            },
            raw: true
        });
    return p;
}

