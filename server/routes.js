var fs = require("fs");
var path = require("path");
var multer = require("multer");

// var storage = multer.diskStorage({
//   // Storage configuration for multer - where to save and under what name
//   // destination: './uploads_tmp/',
//   destination: __dirname + './../client/snapshots',
//   filename: function (req, file, cb) {
//     cb(null, Date.now() + '-' + file.originalname);
//   }
// }),
//   upload = multer({
//     // Actual upload function using previously defined configuration
//     storage: storage
//   });


module.exports = function (auth, app, passport, db) {
  // app.post("/login", function(req, res) {
  //   res.sendStatus(200);
  // });
  console.log('in routes');
  app.post('/login', passport.authenticate('local'), function (req, res) {
    //console.log(req.user);
    res.status(200).json({ user: req.user });
  });

  app.get('/user/auth', function (req, res) {
    if (req.user) {
      res.status(200).json({ user: req.user });
    }
    else {
      res.sendStatus(401);
    }
  });

  app.get('/logout', function (req, res) {
    req.logout();
    req.session.destroy();
    res.redirect('/');
  });

  var isAuthenticated = auth.isAuthenticated;
  var User = require('./api/user.controller')(db);
  app.post('/api/user', User.createUser);
  app.get('/api/user/:id', isAuthenticated, User.retrieveById);
  app.put('/api/user/:id', isAuthenticated, User.updateById);
  app.get('/api/user/', isAuthenticated, User.retrieveByEmail);
  app.delete('/api/user/:id', isAuthenticated, User.deleteUser);

  var Company = require('./api/company.controller')(db);
  app.post('/api/company', isAuthenticated, Company.createCompany); //From coy_reg.html
  app.get('/api/company/:id', isAuthenticated, Company.retrieveById);

  var Gateway = require('./api/gateway.controller')(db);
  app.post('/api/gateway', isAuthenticated, Gateway.createGateway);
  app.get('/api/gateway/company', isAuthenticated,Gateway.retrieveLatestByCompany)
  app.get('/api/gateway', isAuthenticated, Gateway.retrieveList);
  app.delete('/api/gateway/:id', isAuthenticated, Gateway.deleteGateway);

  var P_node = require('./api/p_node.controller')(db);
  app.post('/api/p_node',isAuthenticated,P_node.createNode); 
  app.get('/api/p_node/gateway', isAuthenticated, P_node.retrieveByGatewayId);  
  app.get('/api/p_node/:id', isAuthenticated, P_node.retrieveById);
  app.get('/api/p_node', isAuthenticated, P_node.retrieveByMachineId);
  app.put('/api/p_node/:id', isAuthenticated, P_node.updateById);
  app.put('/api/node/:id', isAuthenticated, P_node.updateFirebase);

  
  // app.post('/snapshot', P_node.snapshot);
  // create an image file to the directory /client/snapshots from server side __Return later to solve modularization
  app.post('/snapshot', function (req, res) {
    var buf = new Buffer(req.body.imagedata, 'base64');
    var filename = Date.now() + '_' + req.body.filename;
    console.log(filename);
    console.log(req.body.imagedata.length);
    fs.writeFile(__dirname + '/../client/snapshots/' + filename, buf);
    res.status(200).json({ savedAsFile: filename });
  });

  var Product_line = require('./api/product_line.controller')(db);
  app.post('/api/product_line', Product_line.createProduct_line); //input req.body where it is the whole object
  app.get('/api/product_line/unassigned',isAuthenticated,Product_line.retrieveUnassigned)
  app.get('/api/product_line/:id', isAuthenticated, Product_line.retrieveById);
  app.put('/api/product_line/:id', isAuthenticated, Product_line.updateById);
  app.get('/api/product_line/', isAuthenticated, Product_line.retrieveList); //by company_id
  app.delete('/api/product_line/:id', isAuthenticated, Product_line.deleteProduct_line);

  var Machine = require('./api/machine.controller')(db);
  app.post('/api/machine', Machine.createMachine);
  app.get('/api/machine/product', isAuthenticated,Machine.retrieveListByLineId);
  app.get('/api/machine/:id', isAuthenticated, Machine.retrieveById);
  //app.get('/api/machine/product', isAuthenticated,Machine.retrieveListByLineId);
  app.put('/api/machine/:id', isAuthenticated, Machine.updateById);


};
