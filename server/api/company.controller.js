//create Company
var createCompany = function (db) {
  return function (req, res) {
    //check CompanyUEN validity Here
    var str = req.body.coy_registry;
    var regex = /^\d{9}[a-zA-Z]$/;
    var valid_num = regex.exec(str);
    //console.log(valid_num);
    if (valid_num) {
      db.Company
        .findOrCreate({
          where: {
            coy_registry: req.body.coy_registry
          },
          defaults: {
            //properties to be created 
            coy_name: req.body.coy_name,
            coy_registry: req.body.coy_registry,
            coy_email: req.body.coy_email
          }
        })
        .spread(function (Company, created) {
          //console.log(Company.get({ plain: true }));
          //console.log("Spread created start:",created);
          if (created == true) {
            
            createNewProduct_Line(db,Company.id)
              .then(function (result) {
                createDefaultMachine(db,result)
                  .then(function (resultMachine) {
                    res
                      .status(200)
                      .json(Company);
                  })
                  .catch(function (err) {
                    console.log("err=", err);
                    var msg = { error: "Cannot create Machine" }
                    res
                      .status(500)
                      .json(msg);
                  });
              })
              .catch(function (err) {
                console.log("err=", err);
                var msg = { error: "Cannot create Product line" }
                res
                  .status(500)
                  .json(msg);
              });

          }
          else {
            //console.log("create error");
            var msg = { error: "Company already exists" }
            res
              .status(500)
              .json(msg);
          }
        });
    }
    else {
      var msg = { error: "Server response: Invalid UEN Format" }
      res
        .status(500)
        .json(msg);
    }
  }
};


// Retrieve from DB
var retrieveByReg = function (db) {
  return function (req, res) {
    db.Company
      .findOne({
        where: {
          coy_registry: req.query.searchString
        }
      })
      .then(function (Company) {
        res
          .status(200)
          .json(Company);
      })
      .catch(function (err) {
        console.log("Company error: " + err);
         var msg = {error: "No company records found"}
        res
          .status(500)
          .json(msg);
      });
  }
};

//Search by name
// var retrieveByName = function (db) {
//   return function (req, res) {
//     if (!req.query.searchString) { req.query.searchString = '' }
//     db.Company
//       .findAll({
//         where: {
//           coy_name: { $like: "%" + req.query.searchString + "%" }
//         },
//         order: [["name", "ASC"]],
//         limit: 20,
//       })
//       .then(function (Company) {
//         res
//           .status(200)
//           .json(Company);
//       })
//       .catch(function (err) {
//         console.log("Name search error clause: " + err);
//         res
//           .status(500)
//           .json(err);
//       });
//   }
// };
//retrieveBy ID
var retrieveById = function (db) {
  return function (req, res) {
    console.log
    var where = {};
    if (req.user.company_id) {
      where.id = parseInt(req.user.company_id)
    }
    console.log("where " + where);
    db.Company
      .findOne({
        where: where
      })
      .then(function (Company) {
        //console.log("-- GET /api/Company/:id findOne then() result \n " + JSON.stringify(Company));
        res.json(Company);
      })
      // this .catch() handles erroneous findAll operation
      .catch(function (err) {
        console.log("-- GET /api/Company/:id findOne catch() \n " + JSON.stringify(Company));
        res
          .status(500)
          .json({ error: true });
      });

  }
};

// -- Updates company info
var updateById = function (db) {
  return function (req, res) {
    var where = {};
    // *** Updates company detail
    db.Company
      .update({
        coy_email: req.body.coy_email,
        coy_registry: req.body.registry,
        coy_name: req.body.coy_name,
      },
      {
        where: {
          id: parseInt(req.body.id) // could also use where as it is defined on line 287
        }
      }
      )
      .then(function (result) {
        console.log(result);
        res
          .status(200)
          .json({ success: result });
      })
      .catch(function (err) {
        console.log("Error");
        console.log(err);
        res
          .status(500)
          .json({ success: false });
      })
  }
};
//create new company and product_line placeholder and machine placeholder
function createNewProduct_Line(db,company_id) {
  var p = db.Product_line
    .create({
      line_name: "unassigned",
      company_id: company_id,
    });
  return p;
}
function createDefaultMachine(db,result) {
  var m = db.Machine
    .create({
      machine_name: 'unassigned',
      machine_image_uri: null,
      machine_sequence: 1,
      product_line_id: result.id
    });
  return m;
}

// Export route handlers
module.exports = function (db) {
  return {
    createCompany: createCompany(db),  //coy_reg.html  
    //retrieveByReg: retrieveByReg(db),
    updateById: updateById(db),
    //retrieveByName: retrieveByName(db),
    retrieveById: retrieveById(db),
    
  }
};
