var createMachine = function (db) {
  return function (req, res) {
    db.Machine
      .findOrCreate({
        where: {
          machine_name: req.body.machine_name,
          product_line_id: req.body.product_line_id
        },
        defaults: {
          //properties to be created 
          machine_name: req.body.machine_name,
          machine_image_uri: req.body.machine_image_uri,
          machine_sequence: 1,
          product_line_id: req.body.product_line_id
        }
      })
      .spread(function (Machine, created) {
        //console.log(Company.get({ plain: true }));
        //console.log("Spread created start:",created);
        if (created == true) {
          var msg = { msg: "Machine successfully added" }
          res
            .status(200)
            .json(Machine);
        }
        else {
          //console.log("Update machine");
          db.Machine
            .update({
              machine_name: req.body.machine_name,
              machine_image_uri: req.body.machine_image_uri,
              machine_sequence: req.body.machine_sequence,
              product_line_id: req.body.product_line_id
            },
            {
              where: {
                machine_name: req.body.machine_name,
                product_line_id: req.body.product_line_id,
              }
            }
            )
            .then(function (result) {
              console.log(result);
              res
                .status(200)
                .json({ success: result });
            })
            .catch(function (err) {
              console.log("Error");
              console.log(err);
              res
                .status(500)
                .json({ success: false });
            })
        }
      });
  }
};

var retrieveById = function (db) {
  return function (req, res) {
    var where = {};
    if (req.params.id) {
      where.id = parseInt(req.params.id)
    }
    console.log("where " + where);
    db.Machine
      .findOne({
        where: where,
        include: [{
          model: db.P_node,
          attributes: ["sensor_name", "node_serial_num","sensor_image_uri"]
        }],
        order: [["id", "ASC"]],
      })
      .then(function (machine) {
        console.log("-- GET /api/machine/:id findOne then() result \n " + JSON.stringify(machine));
        res.json(machine);
      })
      .catch(function (err) {
        console.log("-- GET /api/machine/:id findOne catch() \n " + JSON.stringify(err));
        res
          .status(500)
          .json({ error: true });
      });

  }
};

var retrieveListByLineId = function (db) {
  return function (req, res) {
    console.log("req.query", req.query.product_line_id);
    db.Machine
      .findAll({
        where: {
          product_line_id: req.query.product_line_id
        },
        include: [{
          model: db.Product_line,
          attributes: ["line_name"]
        }],
        order: [["machine_sequence", "ASC"]],
      })
      .then(function (machine) {
        console.log("Machine list based on line", machine)
        res
          .status(200)
          .json(machine);
      })
      .catch(function (err) {
        console.log("Machine error cause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

var updateById = function (db) {
  return function (req, res) {
    console.log(req.body);
    db.Machine
      .update({
        machine_name: req.body.machine_name,
        machine_image_uri: req.body.machine_image_uri,
        machine_sequence: req.body.machine_sequence,
        product_line_id: req.body.product_line_id
      },
      {
        where: {
          id: req.params.id,
        }
      }
      )
      .then(function (result) {
        console.log(result);
        res
          .status(200)
          .json({ success: result });
      })
      .catch(function (err) {
        console.log("Error");
        console.log(err);
        res
          .status(500)
          .json({ success: false });
      })
  }
};

// Export route handlers
module.exports = function (db) {
  return {
    createMachine: createMachine(db),
    retrieveById: retrieveById(db),
    retrieveListByLineId: retrieveListByLineId(db),
    updateById: updateById(db),
  }
};
