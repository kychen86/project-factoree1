// var fs = require('fs');

// var snapshot = function () {
//     return function (req, res) {
//         var buf = new Buffer(req.body.imagedata, 'base64');
//         var filename = Date.now() + '_' + req.body.filename;
//         console.log(filename);
//         console.log(req.body.imagedata.length);
//         fs.writeFile(__dirname + '/../client/snapshots/' + filename, buf);
//         res.status(200).json({ savedAsFile: filename });

//     }
// };
var Firebase = require('firebase');
var config = require('../config');
var Pnode = require("../db").P_node;
var Machine = require("../db").Machine;
var Product_line = require("../db").Product_line;
//create Node
var createNode = function (db) {
  return function (req, res) {
    //check NodeSerial validity Here
    var str = req.body.nodeSN;
    var regex = /^[0-9a-fA-F]{16}$/;
    var valid_num = regex.exec(str);
    //console.log(valid_num);
    if (valid_num) {
      Pnode
        .findOrCreate({
          where: {
            node_serial_num: req.body.nodeSN
          },
          defaults: {
            //properties to be created 
            node_serial_num: req.body.nodeSN,
            sensor_name: req.body.sensor_name,
            sensor_image_uri: req.body.sensor_image_uri,
            sig_type: 3,
            sig_0: 1,
            sig_1: 0,
            sig_2: 0,
            sig_3: 0,
            sig_4: 0,
            sig_5: 0,
            vmax: 24,
            pushint: 1,
            gateway_id: req.body.gateway_id,
            machine_id: req.body.machine_id,
          }
        })
        .spread(function (pnode, created) {
          console.log(pnode.get({ plain: true }));
          console.log(created);
          if (created == true) {
            console.log("ServerSide-------->", pnode.dataValues);
            res
              .status(200)
              .json(pnode);
          }
          else {
            res
              .status(500)
              .json(created);
          }
        });
    }
    else {
      error = { error: "Invalid Serial Number Format" }
      res
        .status(500)
        .json(error);
    }
  }
};

var retrieveByGatewayId = function () {
  return function (req, res) {
    //console.log("In retrieveByGatewayId");
    //console.log(req.query.gateway_id);
    var where = {};
    where.gateway_id = req.query.gateway_id;
    //console.log(where.gateway_id);
    Pnode
      .findAll({
        where: where,
        raw: true,
        include: [{
          model: Machine,
          attributes: ["machine_name"],
          include: [{
            model: Product_line,
            attributes: ["line_name"],
          }]
        }],
        order: [["sensor_name", "ASC"]],
        limit: 50,
      })
      .then(function (p_node) {
        //console.log("Results list based on gateway", p_node)

        for (var i = 0; i < p_node.length; i++) {
          console.log("TESTSIGTYPE:", p_node[i]["sig_type"]);
          p_node[i]["sig_typeName"] = "";
          if (p_node[i]["sig_type"] == 1) {
            p_node[i]["sig_typeName"] = "Count";
          } else if (p_node[i]["sig_type"] == 2) {
            p_node[i]["sig_typeName"] = "State";
          } else {
            p_node[i]["sig_typeName"] = "Analog";
          }
          console.log(p_node[i]["sig_typeName"]);
        }
        res
          .status(200)
          .json(p_node);
      })
      .catch(function (err) {
        console.log("Node error cause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};
var retrieveByMachineId = function (db) {
  return function (req, res) {
    Pnode
      .findAll({
        where: {
          machine_id: req.query.machine_id
        },
        order: [["node_serial_num", "ASC"]],
        limit: 50,
      })
      .then(function (result) {
        console.log("Results list based on machine", result)
        res
          .status(200)
          .json(result);
      })
      .catch(function (err) {
        console.log("Node error cause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

//retrieve by ID
var retrieveById = function (db) {
  return function (req, res) {
    console.log
    var where = {};
    if (req.params.id) {
      where.id = parseInt(req.params.id)
    }
    console.log("where " + where);
    Pnode
      .findOne({
        where: where,
        raw: true,
        include: [{
          model: Machine,
          include: [{
            model: Product_line
          }]
        }],
      })
      .then(function (p_node) {
        console.log("-- GET /api/p_node/:id findOne then() result \n " + p_node);
        p_node.sig_typeName = "";
        if (p_node.sig_type == 1) {
          p_node.sig_typeName = "Count";
        } else if (p_node.sig_type == 2) {
          p_node.sig_typeName = "State";
        } else {
          p_node.sig_typeName = "Analog";
        }

        res.json(p_node);
      })
      .catch(function (err) {
        console.log("-- GET /api/p_node/:id findOne catch() \n " + JSON.stringify(err));
        res
          .status(500)
          .json({ error: true });
      });

  }
};

var updateById = function (db) {
  return function (req, res) {
    console.log("------in update Node by id", JSON.stringify(req.body));
    Pnode
      .update({
        node_serial_num: req.body.node_serial_num,
        sensor_name: req.body.sensor_name,
        sig_type: req.body.sig_type,
        sig_0: req.body.sig_0,
        sig_1: req.body.sig_1,
        sig_2: req.body.sig_2,
        sig_3: req.body.sig_3,
        sig_4: req.body.sig_4,
        sig_5: req.body.sig_5,
        vmax: req.body.vmax,
        pushint: req.body.pushint,
        machine_id: req.body.machine_id
      },
      {
        where: {
          id: parseInt(req.params.id)
        }
      }
      )
      .then(function (result) {
        console.log(result);
        res
          .status(200)
          .json({ success: result });
      })
      .catch(function (err) {
        console.log("Error");
        console.log(err);
        res
          .status(500)
          .json({ success: false });
      })
  }
};

//Firebase 
var updateFirebase = function (db) {
  return function (req, res) {
    console.log("------in update Node by id", JSON.stringify(req.body));
    var serial_num = req.body.node_serial_num;
    var sequence = req.body.machine_sequence;
    var type = req.body.sig_type;
    var name = req.body.sensor_name;
    var pushint = req.body.pushint;
    var vmax = req.body.vmax;
    // Login to Firebase
    var firebaseConfig = {
      apiKey: config.FIREBASE_APIKEY,
      databaseURL: config.FIREBASE_DATABASE_URL
    };
    var myFirebaseRef = Firebase.initializeApp(firebaseConfig);
    myFirebaseRef.auth().signInWithEmailAndPassword('', '').catch(function (err) {
      if (err) console.log(err);
    });
    var node = {};
    serial_num = {
      0: {
        ch: 0,
        field: sequence,
        mode: type
      },
      description: name,
      pushint: pushint,
      vmax: vmax
    };

    var newPostKey = firebase.database().ref().child('node').push().key;
    var updates = {};
    updates['/node/' + newPostKey] = node;
    updates['/node/' + serial_num + '/' + newPostKey] = node;

    return firebase.database().ref().update(updates);
  }
};
//End of TEST FIREBASE


module.exports = function (db) {
  return {
    // snapshot:snapshot(),
    createNode: createNode(db),
    retrieveByGatewayId: retrieveByGatewayId(db),
    retrieveByMachineId: retrieveByMachineId(db),
    retrieveById: retrieveById(db),
    updateById: updateById(db),
    updateFirebase: updateFirebase(),

  }
}




