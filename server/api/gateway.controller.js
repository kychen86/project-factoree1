//create gateway
var createGateway = function (db) {
  return function (req, res) {
    //check GatewaySerial validity Here
    var str = req.body.gatewaySN;
    var regex = /^[0-9a-fA-F]{16}$/;
    var valid_num = regex.exec(str);
    //console.log(valid_num);
    if (valid_num) {
      db.Gateway
        .findOrCreate({
          where: {
            gate_serial_num: req.body.gatewaySN
          },
          defaults: {
            //properties to be created 
            gate_serial_num: req.body.gatewaySN,
            company_id: req.user.company_id
          }
        })
        .spread(function (gateway, created) {
          //console.log(gateway.get({ plain: true }));
          //console.log("Spread created start:",created);
          if (created == true) {
            res
              .status(200)
              .json(gateway);
          }
          else {
            //console.log("create error");
            var msg = {error: "Gateway already exists"}
            res
              .status(500)
              .json(msg);
          }
        });
    }
    else {
      var msg = { error: "Server response: Invalid Serial Number Format" }
      res
        .status(500)
        .json(msg);
    }
  }
};

//delete gateway
var deleteGateway = function (db) {
  return function (req, res) {
    console.log(req.params.id);
    db.Gateway
      .destroy({
        where: {
          id: req.params.id
        }
      })
      .then(function (result) {
        console.log(result);
        res
          .status(200)
          .json(result);
      })
      .catch(function (err) {
        console.log("error: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

//retrieve by ID
var retrieveById = function (db) {
  return function (req, res) {
    console.log
    var where = {};
    if (req.params.id) {
      where.id = parseInt(req.params.id),
        where.company_id = parseInt(req.user.company_id)
    }
    console.log("where " + where);
    db.Gateway
      .findOne({
        where: where
      })
      .then(function (gateway) {
        console.log("-- GET /api/gateway/:id findOne then() result \n " + JSON.stringify(gateway));
        res.json(gateway);
      })
      .catch(function (err) {
        console.log("-- GET /api/gateway/:id findOne catch() \n " + JSON.stringify(gateway));
        res
          .status(500)
          .json({ error: true });
      });

  }
};

// Retrieve a list of gateway
var retrieveList = function (db) {
  return function (req, res) {
    console.log(req.user);
    db.Gateway
      .findAll({
        where: {
          company_id: req.user.company_id
        },
        order: [["gate_serial_num", "ASC"]],
        limit: 50,
      })
      .then(function (gateway) {
        console.log("Gateway list based on company", gateway)
        res
          .status(200)
          .json(gateway);
      })
      .catch(function (err) {
        console.log("Gateway error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

//Search gateway by companyId
var retrieveByCompany = function (db) {
  return function (req, res) {
    db.Gateway
      .findOne({
        where: {
          company_id: req.user.company_id
        }
      })
      .then(function (result) {
        res
          .status(200)
          .json(result);
      })
      .catch(function (err) {
        console.log("Gateway search error: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};
//Search lastest installed gateway by companyId
var retrieveLatestByCompany = function (db) {
     return function (req, res) {
    console.log("Latest------",req.user);
    db.Gateway
      .findAll({
        where: {
          company_id: req.user.company_id
        },
        order: [["createdAt", "DESC"]],
        limit: 1,
      })
      .then(function (gateway) {
        console.log("Latest installed gateway", gateway)
        res
          .status(200)
          .json(gateway);
      })
      .catch(function (err) {
        console.log("Gateway error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};


module.exports = function (db) {
  return {
    retrieveList: retrieveList(db),
    retrieveById: retrieveById(db),
    createGateway: createGateway(db),
    deleteGateway: deleteGateway(db),
    retrieveLatestByCompany : retrieveLatestByCompany(db),
  }
};