// Retrieve from DB
var retrieveUser = function (db) {
  return function (req, res) {
    db.User
      .findAll({
        where: {

        },
        order: [["first_name", "ASC"]],
        limit: 20,
      })
      .then(function (users) {
        res
          .status(200)
          .json(users);
      })
      .catch(function (err) {
        console.log("usersDB error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

//Search by user by email
var retrieveByEmail = function (db) {
  return function (req, res) {
    if (!req.query.email) { req.query.email = '' }
    db.User
      .findOne({
        where: req.query.email
      })
      .then(function (user) {
        res
          .status(200)
          .json(user);
      })
      .catch(function (err) {
        console.log("Email search error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

//retrieveBy ID
var retrieveById = function (db) {
  return function (req, res) {
    console.log(req.params.id);
    var where = {};
    if (req.params.id) {
      where.id = parseInt(req.params.id)
    }
    console.log("where id:" + where.id);
    db.User
      .findOne({
        where: where
      })
      .then(function (data) {
        console.log("-- GET /api/user/:id findOne then() result \n " + JSON.stringify(data));
        res.json(data);
      })
      .catch(function (err) {
        console.log("-- GET /api/user/:id findOne catch() \n " + JSON.stringify(err));
        error = { msg: "ID Error" };
        res
          .status(500)
          .json(error);
      });

  }
};

//Updates user info
var updateById = function (db) {
  return function (req, res) {
    db.User
      .update({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        phone: req.body.phone,
        password_hash: req.body.password_hash
      },
      {
        where: {
          id: parseInt(req.body.id)
        }
      }
      )
      .then(function (result) {
        console.log(result);
        res
          .status(200)
          .json({ success: result });
      })
      .catch(function (err) {
        console.log("Error");
        console.log(err);
        res
          .status(500)
          .json({ success: false });
      })
  }
};
//create user
var createUser = function (db) {
  return function (req, res) {

    var atpos = req.body.email.indexOf("@");
    coy_email = req.body.email.slice(atpos);
    console.log(coy_email);
    db.Company
      .findOne({
        where: {
          coy_email: coy_email
        }
      })
      .then(function (Company) {
        console.log("From create User------",Company);
        
        db.User
          .findOrCreate({
            where: {
              email: req.body.email
            },
            defaults: {
              //properties to be created 
              first_name: req.body.first_name,
              last_name: req.body.last_name,
              password_hash: req.body.password_hash, //Set to clear for now
              email: req.body.email,
              phone: req.body.phone,
              company_id: Company.id,
              sign_up: req.body.sign_up,
              role_id: req.body.role_id,
              reset_password_sent_at: req.body.reset_password_sent_at,
              reset_password_token: req.body.reset_password_token
            }
          })
          .spread(function (User, created) {
            //console.log(User.get({ plain: true }));
            //console.log("Spread created start:",created);
            if (created == true) {
              res
                .status(200)
                .json(User);
            }
            else {
              //console.log("create error");
              var msg = { error: "User already exists" }
              res
                .status(500)
                .json(msg);
            }
          });
      })
      .catch(function (err) {
        console.log("Company error: " + err);
        var msg = { error: "This company is not registered" }
        res
          .status(500)
          .json(msg);
      });
  }
};







//delete user
var deleteUser = function (db) {
  return function (req, res) {
    console.log(req.params.id);
    db.User
      .destroy({
        where: {
          id: parseInt(req.params.id)
        }
      })
      .then(function (result) {
        console.log(result);
        res
          .status(200)
          .json(result);
      })
      .catch(function (err) {
        console.log("error: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};


// Export route handlers
module.exports = function (db) {
  return {
    retrieveUser: retrieveUser(db),
    updateById: updateById(db),
    retrieveByEmail: retrieveByEmail(db),
    retrieveById: retrieveById(db),
    createUser: createUser(db),
    deleteUser: deleteUser(db),
  }
};
