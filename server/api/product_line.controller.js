//create product_line
var createProduct_line = function (db) {
  return function (req, res) {
    // var company_id = "";
    // if ((req.user.company_id == 1) && (req.user.role_id == 1)) {
    //   company_id = req.body.company_id
    // } else {
       company_id = req.user.company_id
    // }
    // console.log(company_id);

    db.Product_line
      .findOrCreate({
        where: {
          line_name: req.body.line_name,
          company_id: company_id,
        },
        defaults: {
          //properties to be created 
          line_name: req.body.line_name,
          company_id: req.user.company_id,
        }
      })
      .spread(function (result, created) {
        //console.log(result.get({ plain: true }));
        //console.log("Spread created start:",created);
        if (created == true) {
          result.msg = "New Product Line Successfully Created";
          res
            .status(200)
            .json(result);
        }
        else {
          //console.log("create error");
          var msg = { error: "Line already exists" }
          res
            .status(500)
            .json(msg);
        }
      });

  }
};

//delete product_line
var deleteProduct_line = function (db) {
  return function (req, res) {
    console.log(req.params.id);
    db.Product_line
      .destroy({
        where: {
          id: req.params.id
        }
      })
      .then(function (result) {
        console.log(result);
        res
          .status(200)
          .json(result);
      })
      .catch(function (err) {
        console.log("error: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

//retrieve by Product_Line ID
var retrieveById = function (db) {
  return function (req, res) {
    var companyId = "";
    if ((req.user.company_id == 1) && (req.user.role_id == 1)) {
      companyId = req.params.company_id
    } else {
      companyId = req.user.company_id
    };
    console.log(companyId);
    var where = {};
    if (req.params.id) {
      where.id = parseInt(req.params.id),
        where.company_id = companyId
    }
    console.log("where " + where);
    db.Product_line
      .findOne({
        where: where
      })
      .then(function (product_line) {
        console.log("-- GET /api/product_line/:id findOne then() result \n " + JSON.stringify(product_line));
        res.json(product_line);
      })
      .catch(function (err) {
        console.log("-- GET /api/product_line/:id findOne catch() \n " + JSON.stringify(product_line));
        res
          .status(500)
          .json({ error: true });
      });

  }
};

// Retrieve a list of product_line based on Company ID
var retrieveList = function (db) {
  return function (req, res) {
    console.log("req.user CompanyID",req.user.company_id);
    var companyId = "";   
    companyId = req.user.company_id;
    console.log(companyId);

    db.Product_line
      .findAll({
        where: {
          company_id: companyId
        },
        include: [{
          model: db.Machine,
          attributes: ["machine_name", "id"],
          order: [["machine_sequence", "ASC"]],
        }],
        order: [["line_name", "ASC"]],
      })
      .then(function (product_line) {
        console.log("Product Line list based on company", product_line)
        res
          .status(200)
          .json(product_line);
      })
      .catch(function (err) {
        console.log("Product line error cause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

//update product_line --only line name can be edited
var updateById = function (db) {
  return function (req, res) {
    var where = {};
    var companyId = "";
    if ((req.user.company_id == 1) && (req.user.role_id == 1)) {
      companyId = req.body.company_id
    } else {
      companyId = req.user.company_id
    };
    console.log(companyId);
    db.Product_line
      .update({
        line_name: req.body.line_name,
      },
      {
        where: {
          id: parseInt(req.body.id),
          company_id: companyId,
        }
      }
      )
      .then(function (result) {
        console.log(result);
        res
          .status(200)
          .json({ success: result });
      })
      .catch(function (err) {
        console.log("Error");
        console.log(err);
        res
          .status(500)
          .json({ success: false });
      })
  }
};

// finding Unassigned Machine via product_line via company
var retrieveUnassigned = function (db) {
return function (req, res) {
    db.Product_line
      .findOne({
        where: {
          company_id: req.user.company_id,
          line_name: "unassigned",
        },
        raw: true,
          include: [{
            model: db.Machine,
            where:{
              machine_name: "unassigned"
            },
            attributes: ["id","machine_name"],
        }],
      })
      .then(function (product_line) {
        //console.log("Results", product_line)
        res
          .status(200)
          .json(product_line);
      })
      .catch(function (err) {
        console.log("Unassigned Machine error cause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};



module.exports = function (db) {
  return {
    retrieveList: retrieveList(db), //By company ID
    retrieveById: retrieveById(db), //By company ID and Product_line ID
    createProduct_line: createProduct_line(db), //Create Line if not found based on company ID and line_name
    deleteProduct_line: deleteProduct_line(db), //Delete Line by ID
    updateById: updateById(db), //Update by Product_line ID and company ID
    retrieveUnassigned: retrieveUnassigned(db),
  }
};