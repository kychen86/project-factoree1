
var express = require("express");
// Creates an instance of express called app
var app = express();
var path = require("path");
var bodyParser = require("body-parser");
var session = require('express-session');
var watch = require('connect-ensure-login');
var passport = require('passport');
var fs = require('fs');
var Firebase = require('firebase');
// Loads sequelize ORM
var Sequelize = require("sequelize"); //can be removed, found in db.js

// CONSTANTS --------------------------------------------------------------------------------------
const config = require('./config');

// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || config.PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
// CLIENT FOLDER is the public directory
const CLIENT_FOLDER = path.join(__dirname, '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');


// Load Database Models
const db = require('./db');

//var mailgun = require('mailgun-js')({ apiKey: config.MAILGUN_API_KEY, domain: config.MAILGUN_DOMAIN });

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(session({
  secret: 'coffee',
  resave: false,
  saveUninitialized: true,
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(CLIENT_FOLDER));

// ROUTE HANDLERS ---------------------------------------------------------------------------------
// Load Routes handlers
var auth = require('./auth')(app, passport);
const routes = require('./routes')(auth, app, passport, db); //this is where the app.js and db.js is inserted as 

// ERROR HANDLING ---------------------------------------------------------------------------------

app.use(function (req, res) {
  res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
  res.status(500).sendFile(path.join(MSG_FOLDER + '/500.html'));
});

// SERVER / PORT SETUP ----------------------------------------------------------------------------
// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
  console.log("Server running at http://localhost:" + NODE_PORT);
});
