
var config = require("./config");
var db = require("./db");
var Company = db.Company;
var Role = db.Role;
var User = db.User;

module.exports = function () {
    if (config.seed) {
        Company
            .create({
                coy_name: "auk industries",
                coy_registry: "111111111C",
                coy_email: "@auk.industries"
            })
            .then(function (coy) {
                console.log(coy);
            }).catch(function () {
                console.log("Error", arguments)
            })

        Company
            .create({
                coy_name: "precision polymer",
                coy_registry: "222222222C",
                coy_email: "@precision.poly"
            })
            .then(function (coy) {
                console.log(coy);
            })
            .catch(function () {
                console.log("Error", arguments)
            })
        Company
            .create({
                coy_name: "awesome foodie",
                coy_registry: "333333333A",
                coy_email: "@awesome.co"
            })
            .then(function (coy) {
                console.log(coy);
            })
            .catch(function () {
                console.log("Error", arguments)
            })
        Company
            .create({
                coy_name: "apple pie",
                coy_registry: "444444444D",
                coy_email: "@pie.apple"
            })
            .then(function (coy) {
                console.log(coy);
            })
            .catch(function () {
                console.log("Error", arguments)
            })
        Company
            .create({
                coy_name: "metal coils pte ltd",
                coy_registry: "555555555G",
                coy_email: "@coils.com"
            })
            .then(function (coy) {
                console.log(coy);
            })
            .catch(function () {
                console.log("Error", arguments)
            })
        Role
            .create({
                role: "admin",
                product_setting: "1",
                report_setting: "1",
                user_setting: "1"
            })
            .then(function (role) {
                console.log(role);
            })
            .catch(function () {
                console.log("Error", arguments)
            })
        Role
            .create({
                role: "basic",
                product_setting: "0",
                report_setting: "0",
                user_setting: "0"
            })
            .then(function (role) {
                console.log(role);
            })
            .catch(function () {
                console.log("Error", arguments)
            })
        User
            .create({
                first_name: "Jo",
                last_name: "Smile",
                password_hash: "123@Auk@123",
                email: "jo@auk.industries",
                phone: "94444444",
                sign_up: "2017-06-12",
                role_id: "1",
                company_id: "1",
            })
        User
            .create({
                first_name: "John",
                last_name: "Small",
                password_hash: "123@Auk@123",
                email: "john@pie.apple",
                phone: "92231286",
                sign_up: "2017-06-12",
                role_id: "1",
                company_id: "4",
            })
        User
            .create({
                first_name: "Tim",
                last_name: "Shaw",
                password_hash: "123@Auk@123",
                email: "tim@pie.apple",
                phone: "91233386",
                sign_up: "2017-06-12",
                role_id: "1",
                company_id: "4",
            })
        User
            .create({
                first_name: "Jim",
                last_name: "Kirk",
                password_hash: "123@Auk@123",
                email: "jim@coils.com",
                phone: "93432623",
                sign_up: "2017-06-12",
                role_id: "1",
                company_id: "5",
            })
    }
};