module.exports = function (sequelize, Sequelize) {
    return sequelize.define("company",
        {
            coy_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            coy_registry: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            coy_email: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
        },
        {
            // options
        });
};