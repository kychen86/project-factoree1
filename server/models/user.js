
//Model for grocery_list table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/

module.exports = function (sequelize, Sequelize) {
    return sequelize.define('user', {
        first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        password_hash: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
        phone: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
        sign_up: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        role_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'roles',
                key: 'id'
            },
        },
        company_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'companies',
                key: 'id'
            },
        },
        reset_password_sent_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        reset_password_token: {
            type: Sequelize.STRING,
            allowNull: true
        }
    });
};