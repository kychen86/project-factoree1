module.exports = function (sequelize, Sequelize) {
    return sequelize.define("gateway",
        {
            gate_serial_num: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            company_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'companies',
                    key: 'id'
                }
            },
        },
        {
            // options
        });
};