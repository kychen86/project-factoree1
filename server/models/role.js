module.exports = function (sequelize, Sequelize) {
    return sequelize.define("role",
        {
            role: {
                type: Sequelize.STRING,
                allowNull: false
            },
            product_setting: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            report_setting: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            user_setting: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            }
        },
        {
            // options
        });
};