module.exports = function (sequelize, Sequelize) {
    return sequelize.define("p_node",
        {
            node_serial_num: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            sensor_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            sensor_image_uri: {
                type: Sequelize.STRING,
                allowNull: true
            },
            sig_type: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            sig_0: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            sig_1: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            sig_2: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            sig_3: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            sig_4: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            sig_5: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            vmax: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            pushint: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            gateway_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'gateways',
                    key: 'id'
                }
            },
            machine_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'machines',
                    key: 'id'
                }
            },
        },
        {
            // options
        });
};