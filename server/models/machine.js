module.exports = function (sequelize, Sequelize) {
    return sequelize.define("machine",
        {
            machine_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            machine_image_uri: {
                type: Sequelize.STRING,
                allowNull: true
            },
            machine_sequence: {
                type: Sequelize.INTEGER,
                allowNull:false
            },
            product_line_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'product_lines',
                    key: 'id'
                }
            },
        },
        {
            // options
        });
};