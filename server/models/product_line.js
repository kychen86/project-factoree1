module.exports = function (sequelize, Sequelize) {
    return sequelize.define("product_line",
        {
            line_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            company_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'companies',
                    key: 'id'
                }
            },
        },
        {
            // options
        });
};