// Read configurations
var config = require('./config');

// Load Sequelize package
var Sequelize = require("sequelize");

// Create Sequelize DB connection
var sequelize = new Sequelize( // input parameters, database,username,password, and options
	'fac3', //'fac1'initialize for aws
	config.MYSQL_USERNAME,
	config.MYSQL_PASSWORD,
	{
		host: config.MYSQL_HOSTNAME,
		port: config.MYSQL_PORT,
		logging: config.MYSQL_LOGGING,
		dialect: 'mysql',
		pool: {	//can be set in config
			max: 5,
			min: 0,
			idle: 10000,
		},
	}
);

// Import DB Models
const User = sequelize.import('./models/user');
const Role = sequelize.import('./models/role');
const Company = sequelize.import('./models/company');
const Gateway = sequelize.import('./models/gateway');
const Machine = sequelize.import('./models/machine');
const P_node = sequelize.import('./models/p_node');
const Product_line = sequelize.import('./models/product_line');

// Define Model Associations
Company.hasMany(User, { foreignKey: 'company_id' });
//User.belongsTo(Company);
Role.hasMany(User, { foreignKey: 'role_id' });
//User.belongsTo(Role);
Company.hasMany(Gateway, { foreignKey: 'company_id' });
//Gateway.belongsTo(Company);
Gateway.hasMany(P_node, { foreignKey: 'gateway_id' });
//P_node.belongsTo(Gateway);
Machine.hasMany(P_node, { foreignKey: 'machine_id' });
P_node.belongsTo(Machine,{ foreignKey: 'machine_id' } );
Product_line.hasMany(Machine, { foreignKey: 'product_line_id' });
Machine.belongsTo(Product_line, { foreignKey: 'product_line_id' });
Company.hasMany(Product_line, { foreignKey: 'company_id' });
//Product_line.belongsTo(Company);
/*
sequelize
	.sync({ force: config.seed })
	.then(function () {
		console.log("Database in Sync Now");
		require("./seed")();
	});
*/
// Exports Models
module.exports = { //setting up the models attached to db.js to be used by others
	// Loads model for departments table
	User: User,
	Role: Role,
	Company: Company,
	Gateway: Gateway,
	P_node: P_node,
	Machine: Machine,
	Product_line: Product_line,
};
