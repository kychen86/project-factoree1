'use strict';

module.exports = {
  domain_name: "http://localhost:3000",
  PORT: 3000,

  MYSQL_USERNAME: 'root',
  MYSQL_PASSWORD: 'fsf_123',
  MYSQL_HOSTNAME: 'localhost',
  MYSQL_PORT: 3306,
  MYSQL_LOGGING: console.log,

  version: '1.0.0',

  seed: false,
  
  MAILGUN_API_KEY: '',
  MAILGUN_DOMAIN: '',

   FIREBASE_APIKEY: "",
  FIREBASE_DATABASE_URL: "",

};